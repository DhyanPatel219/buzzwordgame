package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.ComputingData;
import controller.HangmanController;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;
import java.util.Random;

import static hangman.HangmanProperties.*;
import static javafx.scene.paint.Color.*;
import static settings.AppPropertyType.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    protected Stage          loginStage, helpStage, profilestage;     // the application window
    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits
    VBox vb1, vb2, vb3;
    Button pause;
    protected Scene scene;
    protected BorderPane     appPane;// the root node in the scene graph, to organize the containers
    TextField userNameFld;
    TextField passwordFld;
    TextField username, username1;
    TextField password, password1;
    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane, hb2;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame, cancel;         // the button to start playing a game of Hangman
    HangmanController controller;
    TableColumn lastNameCol;
    Label professsaur;
    Label space = new Label();
    Label methodLabel = new Label();
    Label level = new Label();
    Label timerLabel;
    ComboBox<String> selectFile;
    ToggleButton[] array = new ToggleButton[16];
    private Timeline timeline;
    final static Integer STARTTIME = 60;
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
    String lastLevel;
    Button one, two, three, four, five, six;
    TextArea txtArea1 = new TextArea();
    GridPane gr = new GridPane();
    GridPane gr1 = new GridPane();
    GridPane gr2 = new GridPane();
    GridPane gr3 = new GridPane();
    GridPane gr4 = new GridPane();
    ComputingData cd1 = new ComputingData();
    TextField txtArea = new TextField();
    Button play, next;
    Integer count;
    int value;
    int val;
    TableView<Guess> table;
    Button replay;
    String str = "";
    int arr[] = new int[16];


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        graphicsUtil.Init(app);
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        headPane = new HBox();
        space = new Label("\t\t\t\t\t\t\t\t");
        headPane.getChildren().addAll(guiHeadingLabel, space);
        cancel = gui.initializeChildButton(headPane, CANCEL_ICON.toString(), CANCEL_TOOLTIP.toString(), false);
        headPane.setAlignment(Pos.CENTER_RIGHT);
        headPane.setBackground(new Background(new BackgroundFill(Color.DARKSLATEGRAY, null, null)));
        cancel.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Look, a Confirmation Dialog");
            alert.setContentText("Are you ok with this?");
            gr.setVisible(false);
            gr1.setVisible(false);
            gr2.setVisible(false);
            gr3.setVisible(false);
            gr4.setVisible(false);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                // ... user chose OK
                System.exit(0);

            } else {
                alert.close();
                gr.setVisible(true);
                gr1.setVisible(true);
                gr2.setVisible(true);
                gr3.setVisible(true);
                gr4.setVisible(true);
                // ... user chose CANCEL or closed the dialog
            }
        });

        figurePane = new BorderPane();
        HBox.setHgrow(figurePane, Priority.ALWAYS);
        figurePane.setMinHeight(figurePane.USE_PREF_SIZE);
        figurePane.setPrefHeight(400);
        figurePane.setMaxHeight(figurePane.USE_PREF_SIZE);
//        figurePane.setStyle("-fx-background-color: red;");
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.setStyle("-fx-background-color: black;");
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);
        workspace = new BorderPane();
        workspace.setLeft(toolbar());
        workspace.setTop(headPane);
        workspace.setCenter(draw());
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        for(int i = 0; i<10;i++){
            if(array[i]==null)
                array[i] = new ToggleButton();
        }
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public BorderPane getFigurePane(){
        return figurePane;
    }
    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }

    public VBox toolbar(){
        vb1 = new VBox();
        vb1.setSpacing(10);
        Button loginButton = new Button  ("           Login           ");
        Button newUserButton = new Button("Create New Button");
        loginButton.setOnAction(event -> {
            handleLoginRequest();
        });

        Button help = new Button("       Help       ");
        help.setOnAction(event -> {
            helpScreen();
        });

        final KeyCombination kb = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_ANY);
        final KeyCombination kb1 = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_ANY, KeyCombination.SHIFT_ANY);

        workspace.setOnKeyPressed(e -> {
            if (kb.match(e)) {
                handleLoginRequest();
            }
        });

        workspace.setOnKeyPressed(e -> {
            if (kb1.match(e)) {
                newUserDialog();
            }
        });
        newUserButton.setOnAction(event -> {
            newUserDialog();
        });
        vb1.setAlignment(Pos.BASELINE_CENTER);
        vb1.getChildren().addAll(loginButton, newUserButton, help);
        vb1.setBackground(new Background(new BackgroundFill(DARKGRAY, null, null)));
        return vb1;
    }

    public void handleLoginRequest() {

        loginStage = new Stage();

        Group root = new Group();
        Scene scene = new Scene(root, 300, 150, BLACK);
        loginStage.setScene(scene);

        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(5);
        gridpane.setVgap(5);

        Label userNameLbl = new Label("User Name: ");
        userNameLbl.setTextFill(WHITE);
        gridpane.add(userNameLbl, 0, 1);

        Label passwordLbl = new Label("Password: ");
        passwordLbl.setTextFill(WHITE);
        gridpane.add(passwordLbl, 0, 2);
        username = new TextField();
        username.setPromptText("username");
        gridpane.add(username, 1, 1);

        password = new PasswordField();
        password.setPromptText("password");
        gridpane.add(password, 1, 2);

        Stage finalLoginStage = loginStage;
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                try {
                    if(controller.checkIfKeyValuePairExists(username.getText(), controller.md5(password.getText()))){
                    workspace.setTop(headPane);
                    workspace.setCenter(draw());
                    workspace.setLeft(selectMode());
                    professsaur.setText("Welcome: " + username.getText());
                    }
                    else{
                        System.out.println("Cant");
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                finalLoginStage.close();
            }
        });
        root.getChildren().add(gridpane);

        loginStage.setOpacity(0.7);
        loginStage.initStyle(StageStyle.UNDECORATED);
        loginStage.showAndWait();
    }

    private HBox draw(){
        Canvas canvas;
        GraphicsContext gc;
        HBox draw = new HBox();

        canvas = new Canvas(450, 400);
        gc = canvas.getGraphicsContext2D();

        String hangmanLine = "#000000";
        gc.beginPath();
        gc.stroke();

        gc.setFill(Paint.valueOf(hangmanLine));
        gc.strokeOval(50, 90, 50, 50);
        gc.strokeOval(150, 90, 50, 50);
        gc.strokeOval(250, 90, 50, 50);
        gc.strokeOval(350, 90, 50, 50);
        gc.strokeOval(50, 160, 50, 50);
        gc.strokeOval(150, 160, 50, 50);
        gc.strokeOval(250, 160, 50, 50);
        gc.strokeOval(350, 160, 50, 50);
        gc.strokeOval(50, 230, 50, 50);
        gc.strokeOval(150, 230, 50, 50);
        gc.strokeOval(250, 230, 50, 50);
        gc.strokeOval(350, 230, 50, 50);
        gc.strokeOval(50, 300, 50, 50);
        gc.strokeOval(150, 300, 50, 50);
        gc.strokeOval(250, 300, 50, 50);
        gc.strokeOval(350, 300, 50, 50);
        gc.strokeLine(100, 115, 150, 115);
        gc.strokeLine(100, 185, 150, 185);
        gc.strokeLine(75, 140, 75, 160);
        gc.strokeLine(175, 140, 175, 160);
        gc.strokeLine(300, 255, 350, 255);
        gc.strokeLine(300, 325, 350, 325);
        gc.strokeLine(275, 280, 275, 300);
        gc.strokeLine(375, 280, 375, 300);
        gc.fillText("B", 70, 120);
        gc.fillText("U", 170, 120);
        gc.fillText("Z", 70, 190);
        gc.fillText("Z", 170, 190);
        gc.fillText("W", 270, 260);
        gc.fillText("O", 370, 260);
        gc.fillText("R", 270, 330);
        gc.fillText("D", 370, 330);

        draw.getChildren().addAll(canvas);
        draw.setAlignment(Pos.BASELINE_CENTER);
        return draw;
    }

    public VBox selectMode(){
        vb2 = new VBox();
        vb2.setSpacing(20);
        professsaur = new Label();
        professsaur.setText("Welcome " + username);
        Button playing = new Button("    Start Playing    ");
        Button profile = new Button("         Profile         ");
        profile.setOnAction(event -> {
            try {
                profileScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Label mew1 = new Label();
        Label mew2 = new Label();
        Label mew3 = new Label();
        Label mew4 = new Label();
        Label mew5 = new Label();
        Button logout = new Button("          Logout          ");
        logout.setBackground(new Background(new BackgroundFill(Color.DARKRED, null, null)));
        logout.setTextFill(Color.WHITE);
        logout.setOnAction(event -> {
                workspace.getChildren().clear();
                workspace.setLeft(toolbar());
                workspace.setTop(headPane);
                workspace.setCenter(draw());
        });
        selectFile = new ComboBox<String>();
        selectFile.setPromptText("Select Mode");
        selectFile.getItems().addAll("Animals", "US Cities", "Dictionary", "Fruit");
        vb2.getChildren().addAll(professsaur, selectFile, playing, profile, mew1, mew2, mew3, mew4, mew5, logout);
        vb2.setBackground(new Background(new BackgroundFill(DARKGRAY, null, null)));
        vb2.setAlignment(Pos.BASELINE_CENTER);

        playing.setOnAction(event -> {
            if(selectFile.getValue() == "Animals"||selectFile.getValue() == "US Cities"||selectFile.getValue() == "Dictionary"||selectFile.getValue() == "Fruit") {
                workspace.getChildren().clear();
                workspace.setLeft(levelMode());
                workspace.setCenter(levelSlecetionButton());
                workspace.setTop(headPane);
            } else {
                System.out.println("Select Mode");
            }

            if(selectFile.getValue().equals("Dictionary")){
                int x = controller.getLevelDictionary(username.getText(), selectFile.getValue());
                    if(x == 1){
                    two.setDisable(true);
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    two.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    two.setOpacity(0.5);
                        three.setOpacity(0.5);
                        four.setOpacity(0.5);
                        five.setOpacity(0.5);
                        six.setOpacity(0.5);
                } else if(x == 2){
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                        three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        three.setOpacity(0.5);
                        four.setOpacity(0.5);
                        five.setOpacity(0.5);
                        six.setOpacity(0.5);
                } else if(x == 3){
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                        four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        four.setOpacity(0.5);
                        five.setOpacity(0.5);
                        six.setOpacity(0.5);
                } else if(x == 4){
                    five.setDisable(true);
                    six.setDisable(true);
                        five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        five.setOpacity(0.5);
                        six.setOpacity(0.5);
                } else if(x == 5){
                    six.setDisable(true);
                        six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                        six.setOpacity(0.5);
                }
            } else if(selectFile.getValue().equals("Animals")){
                int y = controller.getLevelAnimals(username.getText(), selectFile.getValue());
                if(y == 1){
                    two.setDisable(true);
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    two.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    two.setOpacity(0.5);
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 2){
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 3){
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 4){
                    five.setDisable(true);
                    six.setDisable(true);
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 5){
                    six.setDisable(true);
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setOpacity(0.5);
                }
            } else if(selectFile.getValue().equals("Fruit")){
                int y = controller.getLevelFruits(username.getText(), selectFile.getValue());
                if(y == 1){
                    two.setDisable(true);
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    two.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    two.setOpacity(0.5);
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 2){
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 3){
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 4){
                    five.setDisable(true);
                    six.setDisable(true);
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if(y == 5){
                    six.setDisable(true);
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setOpacity(0.5);
                }
            } else if(selectFile.getValue().equals("US Cities")) {
                int y = controller.getLevelUsCities(username.getText(), selectFile.getValue());
                if (y == 1) {
                    two.setDisable(true);
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    two.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    two.setOpacity(0.5);
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if (y == 2) {
                    three.setDisable(true);
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    three.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    three.setOpacity(0.5);
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if (y == 3) {
                    four.setDisable(true);
                    five.setDisable(true);
                    six.setDisable(true);
                    four.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    four.setOpacity(0.5);
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if (y == 4) {
                    five.setDisable(true);
                    six.setDisable(true);
                    five.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    five.setOpacity(0.5);
                    six.setOpacity(0.5);
                } else if (y == 5) {
                    six.setDisable(true);
                    six.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
                    six.setOpacity(0.5);
                }
            }
        });

        return vb2;
    }

    public VBox levelMode(){
        vb3 = new VBox();
        vb3.setSpacing(20);
        professsaur = new Label();
        professsaur.setText("Welcome " + username.getText());
        Button help = new Button("       Help       ");
        help.setOnAction(event -> {
            helpScreen();
        });

        Button profile = new Button("         Profile         ");
        profile.setOnAction(event -> {
            try {
                profileScreen();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Label mew1 = new Label();
        Label mew2 = new Label();
        Label mew3 = new Label();
        Label mew4 = new Label();
        Label mew5 = new Label();
        Button logout = new Button("          Logout          ");
        logout.setBackground(new Background(new BackgroundFill(Color.DARKRED, null, null)));
        logout.setTextFill(Color.WHITE);
        logout.setOnAction(event -> {
            workspace.getChildren().clear();
            workspace.setLeft(toolbar());
            workspace.setTop(headPane);
            workspace.setCenter(draw());
        });
        Button home = new Button("       Home       ");

        home.setOnAction(event -> {
            workspace.getChildren().clear();
            workspace.setTop(headPane);
            workspace.setCenter(draw());
            workspace.setLeft(selectMode());
            professsaur.setText("Welcome: " + username.getText());
        });

        final KeyCombination kb1 = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_ANY);

        workspace.setOnKeyPressed(e -> {
            if (kb1.match(e)) {
                workspace.getChildren().clear();
                workspace.setTop(headPane);
                workspace.setCenter(draw());
                workspace.setLeft(selectMode());
                professsaur.setText("Welcome: " + username.getText());
            }
        });

        vb3.getChildren().addAll(professsaur, home, help, profile, mew1, mew2, mew3, mew4, mew5, logout);
        vb3.setBackground(new Background(new BackgroundFill(DARKGRAY, null, null)));
        vb3.setAlignment(Pos.BASELINE_CENTER);

        return vb3;
    }

    private void profileScreen() throws IOException {
        profilestage = new Stage();

        Group root = new Group();
        Scene scene = new Scene(root, 300, 150, BLACK);
        profilestage.setScene(scene);

        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(5);
        gridpane.setVgap(5);

        Label userNameLbl = new Label("User Name: ");
        userNameLbl.setTextFill(WHITE);
        gridpane.add(userNameLbl, 0, 1);

        Label passwordLbl = new Label("Password: ");
        passwordLbl.setTextFill(WHITE);
        gridpane.add(passwordLbl, 0, 2);
        username1 = new TextField();
        username1.setDisable(true);
        username1.setText(username.getText());
        username1.setEditable(false);
        gridpane.add(username1, 1, 1);

        password1 = new TextField();
        password1.setText(password.getText());

        gridpane.add(password1, 1, 2);

        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if(!password1.equals("")){
                    try {
                        controller.update(username1.getText(), controller.md5(password1.getText()));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                profilestage.close();
            }
        });
        root.getChildren().add(gridpane);

        profilestage.setOpacity(0.7);
        profilestage.initStyle(StageStyle.UNDECORATED);
        profilestage.showAndWait();
    }

    private VBox levelSlecetionButton() {

        HBox draw = new HBox();
        HBox draw1 = new HBox();

        one = new Button("1");
        two = new Button("2");
        three = new Button("3");
        four = new Button("4");
        five = new Button("5");
        six = new Button("6");

        one.setShape(new Circle(80));
        two.setShape(new Circle(80));
        three.setShape(new Circle(80));
        four.setShape(new Circle(80));
        five.setShape(new Circle(80));
        six.setShape(new Circle(80));

        one.setMinSize(80, 80);
        two.setMinSize(80, 80);
        three.setMinSize(80, 80);
        four.setMinSize(80, 80);
        five.setMinSize(80, 80);
        six.setMinSize(80, 80);

        one.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));
        two.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));
        three.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));
        four.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));
        five.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));
        six.setBackground(new Background(new BackgroundFill(LIGHTGREEN, null, null)));

        one.setOpacity(0.8);
        two.setOpacity(0.8);
        three.setOpacity(0.8);
        four.setOpacity(0.8);
        five.setOpacity(0.8);
        six.setOpacity(0.8);

        one.setOnAction(event -> {
            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            level.setText("Level 1");
            if(selectFile.getValue().equals("Dictionary")){
                try {
                    workspace.setCenter(playArea(controller.Dictionary()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                try {
////                    controller.saveDictionary(username.getText(), selectFile.getValue(), "2");
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("Animals")) {
//                try {
//                    controller.saveAnimals(username.getText(), selectFile.getValue(), "2");
                    try {
                        workspace.setCenter(playArea(controller.Animals()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("US Cities")){
//                try {
//                    controller.saveUsCities(username.getText(), selectFile.getValue(), "2");
                    try {
                        workspace.setCenter(playArea(controller.UsCities()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }else if(selectFile.getValue().equals("Fruit")){
//                try {
//                    controller.saveFruits(username.getText(), selectFile.getValue(), "2");
                    try {
                        workspace.setCenter(playArea(controller.Fruits()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            if (timeline != null) {
            timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                    new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();
            workspace.setRight(playRightStuff());
        });

        two.setOnAction(event -> {
            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            level.setText("Level 2");
            if(selectFile.getValue().equals("Dictionary")){
//                try {
//                    controller.saveDictionary(username.getText(), selectFile.getValue(), "3");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("Animals")) {
//                try {
//                   try controller.saveAnimals(username.getText(), selectFile.getValue(), "3");
                    try {
                        workspace.setCenter(playArea(controller.Animals()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("US Cities")){
//                try {
//                    controller.saveUsCities(username.getText(), selectFile.getValue(), "3");
                    try {
                        workspace.setCenter(playArea(controller.UsCities()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }else if(selectFile.getValue().equals("Fruit")){
//                try {
//                    controller.saveFruits(username.getText(), selectFile.getValue(), "3");
                    try {
                        workspace.setCenter(playArea(controller.Fruits()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else {
                System.out.println("HAHAHA");
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            workspace.setRight(playRightStuff());
            if (timeline != null) {
                timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                            new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();

        });

        three.setOnAction(event -> {
            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            level.setText("Level 3");
            if(selectFile.getValue().equals("Dictionary")){
//                try {
//                    controller.saveDictionary(username.getText(), selectFile.getValue(), "4");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            } else if(selectFile.getValue().equals("Animals")) {
//                try {
//                    controller.saveAnimals(username.getText(), selectFile.getValue(), "4");
                    try {
                        workspace.setCenter(playArea(controller.Animals()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("US Cities")){
//                try {
//                    controller.saveUsCities(username.getText(), selectFile.getValue(), "4");
                    try {
                        workspace.setCenter(playArea(controller.UsCities()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }else if(selectFile.getValue().equals("Fruit")){
//                try {
//                    controller.saveFruits(username.getText(), selectFile.getValue(), "4");
                    try {
                        workspace.setCenter(playArea(controller.Fruits()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            workspace.setRight(playRightStuff());
            if (timeline != null) {
                timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                            new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();

        });

        four.setOnAction(event -> {

            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            level.setText("Level 4");
            if(selectFile.getValue().equals("Dictionary")){
//                try {
//                    controller.saveDictionary(username.getText(), selectFile.getValue(), "5");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("Animals")) {
//                try {
//                    controller.saveAnimals(username.getText(), selectFile.getValue(), "5");
                    try {
                        workspace.setCenter(playArea(controller.Animals()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("US Cities")){
//                try {
//                    controller.saveUsCities(username.getText(), selectFile.getValue(), "5");
                    try {
                        workspace.setCenter(playArea(controller.UsCities()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }else if(selectFile.getValue().equals("Fruit")){
//                try {
//                    controller.saveFruits(username.getText(), selectFile.getValue(), "5");
                    try {
                        workspace.setCenter(playArea(controller.Fruits()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            workspace.setRight(playRightStuff());
            if (timeline != null) {
                timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                            new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();

        });

        five.setOnAction(event -> {
            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            level.setText("Level 5");

            if(selectFile.getValue().equals("Dictionary")){
//                try {
//                    controller.saveDictionary(username.getText(), selectFile.getValue(), "6");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("Animals")) {
//                try {
//                    controller.saveAnimals(username.getText(), selectFile.getValue(), "6");
                    try {
                        workspace.setCenter(playArea(controller.Animals()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else if(selectFile.getValue().equals("US Cities")){
//                try {
//                    controller.saveUsCities(username.getText(), selectFile.getValue(), "6");
                    try {
                        workspace.setCenter(playArea(controller.UsCities()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }else if(selectFile.getValue().equals("Fruit")){
//                try {
//                    controller.saveFruits(username.getText(), selectFile.getValue(), "6");
                    try {
                        workspace.setCenter(playArea(controller.Fruits()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            workspace.setRight(playRightStuff());
            if (timeline != null) {
                timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                            new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();

        });

        six.setOnAction(event -> {
            workspace.getChildren().clear();
            level.setText("Level 6");
            if(selectFile.getValue().equals("Dictionary")){
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            } else if(selectFile.getValue().equals("Animals")) {
                try {
                    workspace.setCenter(playArea(controller.Animals()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(selectFile.getValue().equals("US Cities")){
                try {
                    workspace.setCenter(playArea(controller.UsCities()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else if(selectFile.getValue().equals("Fruit")){
                try {
                    workspace.setCenter(playArea(controller.Fruits()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            workspace.setLeft(playLeftButtons());
            workspace.setTop(headPane);
            workspace.setRight(playRightStuff());
            if (timeline != null) {
                timeline.stop();
            }
            timeSeconds.set(STARTTIME);
            timeline = new Timeline();
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(STARTTIME+1),
                            new KeyValue(timeSeconds, 0)));
            timeline.playFromStart();

        });


        draw.getChildren().addAll(one, two, three);
        draw.setSpacing(80);
        draw.setAlignment(Pos.CENTER);
        draw1.getChildren().addAll(four, five, six);
        draw1.setSpacing(80);
        draw1.setAlignment(Pos.CENTER);
        VBox main = new VBox();

        methodLabel.setText(selectFile.getValue());
        methodLabel.setAlignment(Pos.CENTER);
        methodLabel.setFont(Font.font(20));


        main.getChildren().addAll(methodLabel, draw, draw1);
        main.setAlignment(Pos.CENTER);
        main.setSpacing(80);
        lastLevel = new String("2");

        return main;
    }

    private VBox playRightStuff() {
        HBox draw = new HBox();
        VBox tableview = new VBox();

        timerLabel = new Label();
        Label timeRemaining = new Label("Time remaining: ");
        timerLabel.setBackground(new Background
                (new BackgroundFill(ALICEBLUE, null, null)));
        timerLabel.textProperty().bind(timeSeconds.asString());
        timerLabel.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if(timerLabel.getText().equals(Integer.toString(0))){
                    gr.setVisible(false);
                    txtArea.setVisible(false);
                    try {
                        endGame();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        timerLabel.setTextFill(RED);
        timerLabel.setStyle("-fx-font-size: 1em;");

        table = new TableView();
        table.setEditable(false);






        table.setMaxHeight(200);
        table.setMaxWidth(190);

        table.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));

        draw.setAlignment(Pos.TOP_CENTER);
        tableview.setAlignment(Pos.BOTTOM_CENTER);
        tableview.getChildren().addAll(table);
        draw.getChildren().addAll( timeRemaining, timerLabel);
        TableColumn firstNameCol = new TableColumn("Total");

        lastNameCol = new TableColumn("Score");
        table.getColumns().addAll(firstNameCol, lastNameCol);
//        txtArea.setEditable(true);

        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Guess,String>("word")
        );
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Guess,Integer>("point")
        );
        final ObservableList<Guess> data = FXCollections.observableArrayList(
        );

//        txtArea.setOnKeyPressed(e -> {
//
//
//            for(int i=0; i<array.length; i++){
//
//
//                    if(!str.equals("")){
//                        if(array[i].getText().equals(str)) {
//                            if (i == 0) {
//                                boolean x = false;
//
//                                if (array[0].getText().equals(str)) {
//                                    if (array[1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[0].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//
//                            } else if (i == 1 || i == 2) {
//                                boolean x = false;
//
//                                if (array[i].getText().equals(str)) {
//                                    if (array[i - 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[i].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//                            } else if (i == 3) {
//                                boolean x = false;
//
//                                if (array[3].getText().equals(str)) {
//                                    if (array[2].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[2].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[6].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[6].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[7].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[7].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[3].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//
//                            } else if (i == 4 || i == 8) {
//                                boolean x = false;
//
//                                if (array[i].getText().equals(str)) {
//                                    if (array[i - 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[i].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//
//                            } else if (i == 12) {
//                                boolean x = false;
//
//                                if (array[12].getText().equals(str)) {
//                                    if (array[8].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[8].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[9].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[9].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[13].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[13].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[12].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//
//                            } else if (i == 13 || i == 14) {
//                                boolean x = false;
//
//                                if (array[i].getText().equals(str)) {
//                                    if (array[i - 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[i].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//                            } else if (i == 15) {
//                                boolean x = false;
//
//                                if (array[15].getText().equals(str)) {
//                                    if (array[1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[15].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//
//                            } else if (i == 7 || i == 11) {
//                                boolean x = false;
//
//                                if (array[i].getText().equals(str)) {
//                                    if (array[i - 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[i].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//                            } else if (i == 5 || i == 6 || i == 9 || i == 10) {
//                                boolean x = false;
//
//                                if (array[i].getText().equals(str)) {
//                                    if (array[i - 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 1].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 1].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i + 3].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i + 3].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 4].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 4].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (array[i - 5].getText().equals(e.getText())) {
//                                        x = true;
//                                        array[i - 5].setBackground((new Background(new BackgroundFill(Color.GREEN, null, null))));
//                                    }
//                                    if (x == false) {
//                                        arr[i] = 0;
//                                        array[i].setBackground((new Background(new BackgroundFill(Color.DARKGRAY, null, null))));
//                                    }
//                                }
//                            }
//
//
//                    }
//                }else if(e.getText().equals(array[i].getText().toLowerCase())){
//                        {
//                            arr[i] = 1;
//                            array[i].setBackground(new Background(new BackgroundFill(Color.PURPLE, null, null)));
//                        }
//                    }
//
//            }
//            str = e.getText();
//        });

        txtArea.setOnKeyPressed(e -> {

            for(int i=0; i<array.length; i++){

                if(e.getText().equals(array[i].getText().toLowerCase())){
                    array[i].setBackground(new Background(new BackgroundFill(Color.PURPLE, null, null)));
                }

            }
        });


        workspace.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                str = "";
//                    System.out.println(cd1.checkWord(txtArea.getText().toUpperCase()));
                    if (cd1.checkWord(txtArea.getText().toUpperCase()) == true) {

                        if (data.size() == 0) {
                            data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));
                        }
                        boolean flag = false;
                        Iterator<Guess> it = data.iterator();
                        count = 0;
                        while (it.hasNext()) {
//                        try {

                            Guess e1 = it.next();
                            count = count + e1.getPoint();
                            if (!e1.getWord().equals(txtArea.getText().toUpperCase())) {
                                flag = true;
                            }

//                        } catch (Exception e1){
//                            e1.getStackTrace();
//                        }
                        }

                        if (flag) {

                            data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));

                            count = count + txtArea.getText().length();
                            val = count;
                        }
                        lastNameCol.setText(String.valueOf(count));
                        table.setItems(data);
                        txtArea.clear();
                    }
//                for(int i=0; i<array.length; i++){
//                        array[i].setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));
//                }
                }else {
                txtArea.clear();
            }
        });

        txtArea.setFont(Font.font(15));
        txtArea.setMaxSize(100, 15);
//        txtArea.setBackground(new Background(new BackgroundFill(Color.DARKBLUE, null, null)));

        txtArea1.setFont(Font.font(15));
        txtArea1.setMaxSize(150, 50);
        txtArea1.setBackground(new Background(new BackgroundFill(Color.DARKBLUE, null, null)));
        value = cd1.targetScore();
        if(level.getText().equals("Level 1") || level.getText().equals("Level 2") || level.getText().equals("Level 3")){
            txtArea1.setText("Target: " + value/2);
        } else {
            txtArea1.setText("Target: " + value);
        }
        txtArea1.setEditable(false);

        next = new Button("Next Level");
        next.setVisible(false);
        VBox main = new VBox();
        main.setSpacing(20);
        main.getChildren().addAll(draw, txtArea, table, txtArea1, next);
        main.setAlignment(Pos.CENTER);

        return main;
    }

    private void endGame() throws IOException {
        txtArea.setVisible(false);
        play.setVisible(false);
        pause.setVisible(false);
        System.out.println(val);
        if(val >= cd1.targetScore()/2){
            System.out.println("You win");
            if(level.getText().equals("Level 1") && selectFile.getValue().equals("Dictionary")){
                controller.saveDictionary(username.getText(), selectFile.getValue(), "2");
                next.setVisible(true);
                next.setOnAction(event -> {
                        workspace.getChildren().clear();
                        level.setText("Level 2");
                        methodLabel.setText("Dictionary");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    txtArea.setVisible(true);
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);

                });
            } else if(level.getText().equals("Level 2") && selectFile.getValue().equals("Dictionary")){
                controller.saveDictionary(username.getText(), selectFile.getValue(), "3");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 3");
                    methodLabel.setText("Dictionary");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 3") && selectFile.getValue().equals("Dictionary")){
                controller.saveDictionary(username.getText(), selectFile.getValue(), "4");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 4");
                    methodLabel.setText("Dictionary");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();

                });
            } else if(level.getText().equals("Level 4") && selectFile.getValue().equals("Dictionary")){
                controller.saveDictionary(username.getText(), selectFile.getValue(), "5");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 5");
                    methodLabel.setText("Dictionary");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 5") && selectFile.getValue().equals("Dictionary")){
                controller.saveDictionary(username.getText(), selectFile.getValue(), "6");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 6");
                    methodLabel.setText("Dictionary");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 1") && selectFile.getValue().equals("Fruit")){
                controller.saveFruits(username.getText(), selectFile.getValue(), "2");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 2");
                    methodLabel.setText("Fruit");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 2") && selectFile.getValue().equals("Fruit")){
                controller.saveFruits(username.getText(), selectFile.getValue(), "3");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 3");
                    methodLabel.setText("Fruit");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 3") && selectFile.getValue().equals("Fruit")){
                controller.saveFruits(username.getText(), selectFile.getValue(), "4");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 4");
                    methodLabel.setText("Fruit");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 4") && selectFile.getValue().equals("Fruit")){
                controller.saveFruits(username.getText(), selectFile.getValue(), "5");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 5");
                    methodLabel.setText("Fruit");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 5") && selectFile.getValue().equals("Fruit")){
                controller.saveFruits(username.getText(), selectFile.getValue(), "6");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 6");
                    methodLabel.setText("Fruit");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 1") && selectFile.getValue().equals("US Cities")){
                controller.saveUsCities(username.getText(), selectFile.getValue(), "2");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 2");
                    methodLabel.setText("US Cities");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 2") && selectFile.getValue().equals("US Cities")){
                controller.saveUsCities(username.getText(), selectFile.getValue(), "3");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 3");
                    methodLabel.setText("US Cities");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 3") && selectFile.getValue().equals("US Cities")){
                controller.saveUsCities(username.getText(), selectFile.getValue(), "4");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 4");
                    methodLabel.setText("US Cities");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 4") && selectFile.getValue().equals("US Cities")){
                controller.saveUsCities(username.getText(), selectFile.getValue(), "5");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 5");
                    methodLabel.setText("US Cities");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 5") && selectFile.getValue().equals("US Cities")){
                controller.saveUsCities(username.getText(), selectFile.getValue(), "6");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 6");
                    methodLabel.setText("US Cities");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 1") && selectFile.getValue().equals("Animals")){
                controller.saveAnimals(username.getText(), selectFile.getValue(), "2");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 2");
                    methodLabel.setText("Animals");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 2") && selectFile.getValue().equals("Animals")){
                controller.saveAnimals(username.getText(), selectFile.getValue(), "3");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 3");
                    methodLabel.setText("Animals");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 3") && selectFile.getValue().equals("Animals")){
                controller.saveAnimals(username.getText(), selectFile.getValue(), "4");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 4");
                    methodLabel.setText("Animals");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 4") && selectFile.getValue().equals("Animals")){
                controller.saveAnimals(username.getText(), selectFile.getValue(), "5");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 5");
                    methodLabel.setText("Animals");
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);
                    timerDisplay();
                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            } else if(level.getText().equals("Level 5") && selectFile.getValue().equals("Animlas")){
                controller.saveAnimals(username.getText(), selectFile.getValue(), "6");
                next.setVisible(true);
                next.setOnAction(event -> {
                    workspace.getChildren().clear();
                    level.setText("Level 6");
                    methodLabel.setText("Animals");
                    timerDisplay();
                    try {
                        workspace.setCenter(playArea(controller.Dictionary()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtArea.setVisible(true);

                    altToEnter();
                    workspace.setRight(playRightStuff());
                    workspace.setLeft(playLeftButtons());
                    workspace.setTop(headPane);
                    replay.fire();
                });
            }
        } else {

            System.out.println("You lose");
            helpStage = new Stage();

            Group root = new Group();
            Scene scene = new Scene(root, 500, 200, BLACK);
            helpStage.setScene(scene);
            TextArea ta = new TextArea();
            ta.setEditable(false);
            for(int i=0; i< cd1.getWords().size(); i++) {
                ta.appendText(cd1.getWords().get(i) +"\n");
            }
            ScrollPane sp = new ScrollPane();
            sp.setContent(ta);
            scene.setOnKeyPressed(e -> {
                if (e.getCode() == KeyCode.ALT) {
                    helpStage.close();
                }
            });
            root.getChildren().add(sp);
//        helpStage.setOpacity(0.7);
            helpStage.initStyle(StageStyle.UNDECORATED);
            helpStage.show();
//            helpStage.showAndWait();

        }
    }

    private VBox playLeftButtons() {
        VBox vb6 = new VBox();
        vb6.setSpacing(20);
        professsaur = new Label();
        professsaur.setText("Welcome " + username.getText());
        Button home = new Button("       Home       ");
        Button help = new Button("       Help       ");
        help.setOnAction(event -> {
            helpScreen();
        });
        replay = new Button("       ReplayLevel       ");
        TableColumn lastNameCol = new TableColumn("Score");

        final ObservableList<Guess> data = FXCollections.observableArrayList(
        );

    replay.setOnAction(event -> {
        workspace.getChildren().clear();
        gr.setVisible(true);
        txtArea.setVisible(true);
        str = "";
        if(level.getText().equals("Level 1")){
            one.fire();
        } else if(level.getText().equals("Level 2")){
            two.fire();
        } else if(level.getText().equals("Level 3")){
            three.fire();
        } else if(level.getText().equals("Level 4")){
            four.fire();
        } else if(level.getText().equals("Level 5")){
            five.fire();
        } else if(level.getText().equals("Level 6")){
            six.fire();
        }
    });
        Label mew1 = new Label();
        Label mew2 = new Label();
        Label mew3 = new Label();
        Label mew4 = new Label();
        Label mew5 = new Label();
        Button logout = new Button("          Logout          ");
        logout.setBackground(new Background(new BackgroundFill(Color.DARKRED, null, null)));
        logout.setTextFill(Color.WHITE);

        logout.setOnAction(event -> {
            workspace.getChildren().clear();
            workspace.setLeft(toolbar());
            workspace.setTop(headPane);
            workspace.setCenter(draw());
        });

        home.setOnAction(event -> {
            workspace.getChildren().clear();
            gr.setVisible(true);
            txtArea.setVisible(true);
            workspace.setTop(headPane);
            workspace.setCenter(draw());
            workspace.setLeft(selectMode());
            professsaur.setText("Welcome: " + username.getText());
        });

        final KeyCombination kb1 = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_ANY);

        workspace.setOnKeyPressed(e -> {
            if (kb1.match(e)) {
                workspace.getChildren().clear();
                workspace.setTop(headPane);
                workspace.setCenter(draw());
                workspace.setLeft(selectMode());
                professsaur.setText("Welcome: " + username.getText());
            }
        });

        vb6.getChildren().addAll(professsaur, home, help, replay, mew1, mew2, mew3, mew4, mew5, logout);
        vb6.setBackground(new Background(new BackgroundFill(DARKGRAY, null, null)));
        vb6.setAlignment(Pos.CENTER);

        return vb6;
    }

    private void helpScreen() {
        helpStage = new Stage();

        Group root = new Group();
        Scene scene = new Scene(root, 500, 200);
        helpStage.setScene(scene);
        TextArea ta = new TextArea();

        ta.setEditable(false);
        ta.setText("First, divide everyone into two teams. On y" +
                "our " + "\n" + "team's turn, one member attempts to give "+"\n"+"clues to 10 words. T" +
                "he cards state its buzzword. " + "\n" + "Other team members must solve "+"\n"+"each clue by saying " +
                "the phrases that includes the buzzword. " + "\n" + "Meanwhile, the other team keeps"+"\n"+" track of the time and the" +
                " missed clues. " + "\n" + "Score one point for each correctly solved clue. The clue"+"\n"+" giver reads the missed clues to t" +
                "he other team, giving them a chance to score. " + "\n" + "First team to reach 5" +
                "0 points wins"+"\n"+". Watch out! You only get 45 sec" +
                "onds to complete "+"\n"+"your answers to questions ranging " + "\n" + "from easy to quite difficult.");
        ta.setWrapText(true);
        ScrollPane sp = new ScrollPane();
        sp.setContent(ta);
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ALT) {
                helpStage.close();
            }
        });
        ta.setMaxSize(500, 250);
        root.getChildren().add(ta);
//        helpStage.setOpacity(0.7);
        helpStage.initStyle(StageStyle.UNDECORATED);
        helpStage.showAndWait();
    }

    private VBox playArea(String str) throws IOException {
        VBox main = new VBox();
        str = controller.randomLetters();
        System.out.println(str.length() + " " + str);
        for(int i = 0; i <= 15; i ++) {
        ToggleButton bt = new ToggleButton(String.valueOf(i + 1));
        bt.setText(String.valueOf(str.charAt(i)));
        bt.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));
        bt.setOpacity(0.8);
        bt.setTextFill(Color.WHITE);
        bt.setMinSize(80, 80);
        bt.setShape(new Circle(80));
        bt.setDisable(false);
//        bt.setEffect(new Glow(0.3));
        array[i] = bt;

            bt.setOnMouseDragEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(!bt.isSelected()){
                        bt.setSelected(true);
                        bt.setBackground(new Background(new BackgroundFill(Color.GREEN, null, null)));
                        txtArea.appendText(bt.getText());
                    }
                }
            });
        }



        VBox vb4 = new VBox();
//
        int toSkip = new Random().nextInt(2);
        char board [][] = new char[4][];

        toSkip = 0;
        if(toSkip == 0){
            gr.add(array[0], 1, 0);
            gr.add(array[1], 2, 0);
            gr.add(array[2], 3, 0);
            gr.add(array[3], 4, 0);
            String word = (array[0].getText() + array[1].getText() + array[2].getText() + array[3].getText());
            System.out.println(word);
            board[0] = word.toCharArray();

            gr.add(array[4], 1, 1);
            gr.add(array[5], 2, 1);
            gr.add(array[6], 3, 1);
            gr.add(array[7], 4, 1);
             word = (array[4].getText() + array[5].getText() + array[6].getText() + array[7].getText());
            System.out.println(word);
            board[1] = word.toCharArray();

            gr.add(array[8], 1, 2);
            gr.add(array[9], 2, 2);
            gr.add(array[10], 3, 2);
            gr.add(array[11], 4, 2);
             word = (array[8].getText() + array[9].getText() + array[10].getText() + array[11].getText());
            System.out.println(word);
            board[2] = word.toCharArray();

            gr.add(array[12], 1, 3);
            gr.add(array[13], 2, 3);
            gr.add(array[14], 3, 3);
            gr.add(array[15], 4, 3);
             word = (array[12].getText() + array[13].getText() + array[14].getText() + array[15].getText());
            System.out.println(word);
            board[3] = word.toCharArray();



//            System.out.print("Following words of dictionary are present\n");
//            controller.findWords(board);
            char[] str1 = {board[0][0],board[0][1],board[0][2], board[0][3],board[1][0],board[1][1],board[1][2], board[1][3],
                    board[2][0],board[2][1],board[2][2], board[2][3],board[3][0],board[3][1],board[3][2], board[3][3]};
            cd1.recursiveSolveCaller(String.valueOf(str1));
//            List<String> list = controller.boggleSolver(board);
//            for (String str1 :  list) {
//                System.out.println( str1);
//            }

//            System.out.println("Finished");

            gr.setHgap(20);
            gr.setVgap(20);

            gr.setAlignment(Pos.CENTER);
            vb4.getChildren().addAll(gr);
        } else if(toSkip == 1) {
            gr1.add(array[0], 2, 2);
            gr1.add(array[1], 3, 3);
            gr1.add(array[2], 4, 4);
            gr1.add(array[3], 3, 4);

            gr1.add(array[7], 1 , 3);
            gr1.add(array[6], 1 , 4);
            gr1.add(array[5], 2 , 3);
            gr1.add(array[4], 2 , 4);

            gr1.add(array[8], 1 , 2);
            gr1.add(array[9], 1 , 1);
            gr1.add(array[10], 2 , 1);
            gr1.add(array[11], 3 , 2);

            gr1.add(array[15], 4 , 3);
            gr1.add(array[14], 4 , 2);
            gr1.add(array[13], 3 , 1);
            gr1.add(array[12], 4 , 1);

            gr1.setHgap(20);
            gr1.setVgap(20);

            gr1.setAlignment(Pos.CENTER);
            vb4.getChildren().addAll(gr1);
        } else if(toSkip == 2){
            gr2.add(array[0], 1, 4);
            gr2.add(array[1], 2, 4);
            gr2.add(array[2], 3, 3);
            gr2.add(array[3], 4, 2);

            gr2.add(array[7], 3 , 4);
            gr2.add(array[6], 4 , 4);
            gr2.add(array[5], 4 , 3);
            gr2.add(array[4], 3 , 2);

            gr2.add(array[8], 2 , 3);
            gr2.add(array[9], 1 , 3);
            gr2.add(array[10], 2 , 2);
            gr2.add(array[11], 1 , 2);

            gr2.add(array[15], 4 , 1);
            gr2.add(array[14], 3 , 1);
            gr2.add(array[13], 2 , 1);
            gr2.add(array[12], 1 , 1);

            gr2.setHgap(20);
            gr2.setVgap(20);

            gr2.setAlignment(Pos.CENTER);
            vb4.getChildren().addAll(gr2);
        } else if(toSkip == 3){
            gr3.add(array[0], 1, 3);
            gr3.add(array[1], 2, 2);
            gr3.add(array[2], 2, 1);
            gr3.add(array[3], 1, 1);

            gr3.add(array[7], 2 , 4);
            gr3.add(array[6], 1 , 4);
            gr3.add(array[5], 2 , 3);
            gr3.add(array[4], 1 , 2);

            gr3.add(array[8], 3 , 3);
            gr3.add(array[9], 4 , 4);
            gr3.add(array[10], 3 , 4);
            gr3.add(array[11], 4 , 3);

            gr3.add(array[15], 4 , 2);
            gr3.add(array[14], 3 , 1);
            gr3.add(array[13], 4 , 1);
            gr3.add(array[12], 3 , 2);

            gr3.setHgap(20);
            gr3.setVgap(20);

            gr3.setAlignment(Pos.CENTER);
            vb4.getChildren().addAll(gr3);
        } else if(toSkip == 4){
            gr4.add(array[0], 4, 3);
            gr4.add(array[1], 4, 4);
            gr4.add(array[2], 3, 3);
            gr4.add(array[3], 4, 2);

            gr4.add(array[7], 3 , 1);
            gr4.add(array[6], 2 , 2);
            gr4.add(array[5], 3 , 2);
            gr4.add(array[4], 4 , 1);

            gr4.add(array[8], 2 , 1);
            gr4.add(array[9], 1 , 1);
            gr4.add(array[10], 1 , 2);
            gr4.add(array[11], 2 , 3);

            gr4.add(array[15], 3 , 4);
            gr4.add(array[14], 2 , 4);
            gr4.add(array[13], 1 , 3);
            gr4.add(array[12], 1 , 4);

            gr4.setHgap(20);
            gr4.setVgap(20);

            gr4.setAlignment(Pos.CENTER);
            vb4.getChildren().addAll(gr4);
        } else{
            System.out.print("Not getting it random");
        }
//        vb4.getChildren().addAll(gr);
//        vb4.getChildren().addAll(gr);
        vb4.setAlignment(Pos.CENTER);
        vb4.setSpacing(10);
        level.setFont(Font.font(20));
        level.underlineProperty();
        main.getChildren().addAll(methodLabel, vb4, level);
        main.setSpacing(30);

        play = gui.initializeChildButton(main, PLAY_ICON.toString(), PLAY_TOOLTIP.toString(), false);
        pause = gui.initializeChildButton(main, EXIT_ICON.toString(), PLAY_TOOLTIP.toString(), false);
//        play.setVisible(true);

//        gr.setVisible(false);
//        pause.setVisible(false);
        play.setVisible(false);
        play.setAlignment(Pos.CENTER);
        play.setOnAction(event -> {
            play.setVisible(false);
            pause.setVisible(true);
            gr.setVisible(true);
            txtArea.setEditable(true);
            timeline.play();
        });
        pause.setOnAction(event -> {
            play.setVisible(true);
            pause.setVisible(false);
            gr.setVisible(false);
            txtArea.setEditable(false);
            timeline.pause();
        });

        vb4.addEventFilter(MouseEvent.DRAG_DETECTED , new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {
            vb4.startFullDrag();
//            System.out.println("dragging");
        }
        });

        final ObservableList<Guess> data = FXCollections.observableArrayList(
        );

        vb4.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {
//        System.out.println("exited");
        Event.fireEvent(vb4 , new MouseEvent(MouseEvent.MOUSE_RELEASED, 0, 0, 0, 0, MouseButton.PRIMARY, 1,
                        true, true, true, true, true, true, true, true, true, true, null));
            {

                if (data.size() == 0 && txtArea.getText().length() != 0) {
                    data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));
                }
                boolean flag = false;
                Iterator<Guess> it = data.iterator();
                count = 0;
                while (it.hasNext()) {
//                        try {

                    Guess e1 = it.next();
                    count = count + e1.getPoint();
                    if (!e1.getWord().equals(txtArea.getText().toUpperCase())) {
                        flag = true;
                    }

//                        } catch (Exception e1){
//                            e1.getStackTrace();
//                        }
                }

                if (flag) {
                    if(txtArea.getText().length() != 0) {
                        data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));

                        count = count + txtArea.getText().length();
                        val = count;
                    }
                }
                lastNameCol.setText(String.valueOf(count));
                if(txtArea.getText().length() != 0){
                    table.setItems(data);
                }

                txtArea.clear();
            }
        }
        });


        vb4.addEventFilter(MouseEvent.MOUSE_RELEASED , new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
//    System.out.println("released");
        for(int i=0; i<array.length; i++){
    array[i].setSelected(false);
    array[i].setBackground(new Background(new BackgroundFill(Color.DARKGRAY, null, null)));

        }
        if (cd1.checkWord(txtArea.getText().toUpperCase()) == true) {

            if (data.size() == 0) {
                data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));
            }
            boolean flag = false;
            Iterator<Guess> it = data.iterator();
            count = 0;
            while (it.hasNext()) {
//                        try {

                Guess e1 = it.next();
                count = count + e1.getPoint();
                if (!e1.getWord().equals(txtArea.getText().toUpperCase())) {
                    flag = true;
                }

//                        } catch (Exception e1){
//                            e1.getStackTrace();
//                        }
            }

            if (flag) {

                data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));

                count = count + txtArea.getText().length();
                val = count;
            }
            lastNameCol.setText(String.valueOf(count));
            table.setItems(data);
            txtArea.clear();
        }else {
            txtArea.clear();
        }
    }
        });

        main.setAlignment(Pos.CENTER);
        return main;
    }

    private void newUserDialog() {
        loginStage = new Stage();

        Group root = new Group();
        Scene scene = new Scene(root, 300, 150, BLACK);
        loginStage.setScene(scene);

        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(5);
        gridpane.setVgap(5);

        Label userNameLbl = new Label("User Name: ");
        gridpane.add(userNameLbl, 0, 1);

        Label passwordLbl = new Label("Password: ");
        gridpane.add(passwordLbl, 0, 2);

        userNameLbl.setTextFill(Color.WHITE);
        passwordLbl.setTextFill(Color.WHITE);

        userNameFld = new TextField();
        userNameFld.setPromptText("Username");
        gridpane.add(userNameFld, 1, 1);

        passwordFld = new PasswordField();
        passwordFld.setPromptText("Password");
        gridpane.add(passwordFld, 1, 2);

        Stage finalLoginStage = loginStage;
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                try {
                    String str = new String(userNameFld.getText());
                    String pss = new String(passwordFld.getText());
                    if(!passwordFld.getText().equals("") && !userNameFld.getText().equals("")) {
                        controller.save(str, controller.md5(pss));
                        controller.saveDictionary(userNameFld.getText(), "Dictionary", "1");
                        controller.saveFruits(userNameFld.getText(), "Fruit", "1");
                        controller.saveUsCities(userNameFld.getText(), "US Cities", "1");
                        controller.saveAnimals(userNameFld.getText(), "Animals", "1");
                    } else {
                        System.out.println("Enter password or username");
                    }
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                finalLoginStage.close();
            }
        });
        loginStage.setOpacity(0.7);
        loginStage.initStyle(StageStyle.UNDECORATED);

        root.getChildren().add(gridpane);
        loginStage.showAndWait();
    }

    public static class Guess {
        private final SimpleStringProperty word;
        private final SimpleIntegerProperty point;

        private Guess(String wrd, Integer score) {
            this.word = new SimpleStringProperty(wrd);
            this.point = new SimpleIntegerProperty(score);
        }
        public String getWord() {
            return word.get();
        }
        public void setWord(String fName) {
            word.set(fName);
        }

        public Integer getPoint() {
            return point.get();
        }
        public void setPoint(int fName) {
            point.set(fName);
        }

    }

    public void timerDisplay(){
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME+1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();
    }

    public void altToEnter(){
        TableColumn firstNameCol = new TableColumn("Total");

        TableColumn lastNameCol = new TableColumn("Score");
        table.getColumns().addAll(firstNameCol, lastNameCol);
//        txtArea.setEditable(true);

        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Guess,String>("word")
        );
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Guess,Integer>("point")
        );
        final ObservableList<Guess> data = FXCollections.observableArrayList(
        );

        workspace.setOnKeyPressed(e -> {
            if(timeline.statusProperty().getValue().toString().equals("STOPPED"))
                try {
                    endGame();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            if (e.getCode() == KeyCode.ALT) {
                System.out.println(cd1.checkWord(txtArea.getText().toUpperCase()));
                if (cd1.checkWord(txtArea.getText().toUpperCase()) == true) {

                    if (data.size() == 0) {
                        data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));
                    }
                    boolean flag = false;
                    Iterator<Guess> it = data.iterator();
                    count = 0;
                    while (it.hasNext()) {
//                        try {

                        Guess e1 = it.next();
                        count = count + e1.getPoint();
                        if (!e1.getWord().equals(txtArea.getText().toUpperCase())) {
                            flag = true;
                        }

//                        } catch (Exception e1){
//                            e1.getStackTrace();
//                        }
                    }

                    if (flag) {

                        data.add(new Guess(txtArea.getText().toUpperCase(), txtArea.getText().length()));

                        count = count + txtArea.getText().length();
                        val = count;
                    }
                    lastNameCol.setText(String.valueOf(count));
                    table.setItems(data);
                    txtArea.clear();
                }
            }
        });
    }
}
