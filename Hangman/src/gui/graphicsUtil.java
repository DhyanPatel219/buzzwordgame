package gui;

import apptemplate.AppTemplate;
import data.GameData;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Paint;

/**
 * Created by Dhyan on 10/5/16.
 */
public class graphicsUtil {

    static GameData gm;
    static AppTemplate appTemplate;
    static Workspace gameWorkspace;
    protected static Canvas canvas;
    protected static GraphicsContext gc;

    public static void Init(AppTemplate app)
    {
       graphicsUtil.appTemplate = app;
    }

    public static void draw(){
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gm = (GameData) appTemplate.getDataComponent();
        BorderPane figure = gameWorkspace.figurePane;
//        figure.setStyle("-fx-background-color: red;");
        canvas = new Canvas(450, 400);
//        figure.getChildren().addAll(canvas);
        gc = canvas.getGraphicsContext2D();
//        canvas.setStyle("-fx-background-color: blue");

//        gc.setColor(Color.BLACK);

//        if(gm.getRemainingGuesses() < 9)
//            gc.drawRect(10,70, 50, 5);
//        if(gm.getRemainingGuesses() < 8)
//            gc.drawLine(35, 70, 35, 5);
//        if(gm.getRemainingGuesses() < 7)
//            gc.drawLine(35,5,70,5);
//        if(gm.getRemainingGuesses() < 6)
//            gc.drawLine(70,60,90,65);
//        if(gm.getRemainingGuesses() < 5)
//            gc.drawLine(70, 30, 50, 25);
//        if(gm.getRemainingGuesses() < 4)
//            gc.drawLine(70,60,50,65);
//            gc.drawLine(70, 30, 90, 25);
//        if(gm.getRemainingGuesses() < 3)
//            gc.drawLine(70,60,90,65);
//        if(gm.getRemainingGuesses() < 2)
//            gc.drawLine(70, 30, 70, 60);
//        if(gm.getRemainingGuesses() < 1)
//            gc.drawLine(70, 30, 90, 25);

        String hangmanLine = "#000000";
        double lineW = 2.5;
        int lineH = 150;
        double lineW1 = 2.5;
        int lineH1 = 22;
//        gc.setFill(Paint.valueOf(hangmanLine));
        gc.beginPath();
        gc.stroke();

//        gc.setFill(Paint.valueOf(hangmanLine));
//        gc.strokeLine(37, 360, 278, 360);
//        gc.strokeLine(105, 360, 105, 90);
//        gc.strokeLine(105, 90, 210, 90);
//        gc.strokeLine(210, 90, 210, 110);
//        gc.strokeOval(186, 110, 50, 50);
//        gc.strokeLine(210, 160, 210, 245);
//        gc.strokeLine(210, 190, 170, 230);
//        gc.strokeLine(210, 190, 250, 230);
//        gc.strokeLine(210, 245, 170, 285);
//        gc.strokeLine(210, 245, 250, 285);
        gc.setLineWidth(2.5);
        if(gm.getRemainingGuesses() <= 9) {
            gc.strokeLine(37, 360, 278, 360);
        }
        if(gm.getRemainingGuesses() <= 8){
            gc.strokeLine(105, 360, 105, 90);
        }
        if(gm.getRemainingGuesses() <= 7){
            gc.strokeLine(105, 90, 210, 90);
        }
        if(gm.getRemainingGuesses() <= 6){
            gc.strokeLine(210, 90, 210, 110);
        }
        if(gm.getRemainingGuesses() <= 5){
            gc.strokeOval(186, 110, 50, 50);
        }
        if(gm.getRemainingGuesses() <= 4){
            gc.strokeLine(210, 160, 210, 245);
        }
        if(gm.getRemainingGuesses() <= 3){
            gc.strokeLine(210, 190, 170, 230);
        }
        if(gm.getRemainingGuesses() <= 2){
            gc.strokeLine(210, 190, 250, 230);
        }
        if(gm.getRemainingGuesses() <= 1){
            gc.strokeLine(210, 245, 170, 285);
        }
        if(gm.getRemainingGuesses() <= 0){
            gc.strokeLine(210, 245, 250, 285);
        }

//        if(gm.getRemainingGuesses() <= 8)
//        gc.fillRect(100, 250, lineW, lineH);
//        if(gm.getRemainingGuesses() <= 7)
//        gc.fillRect(100, 100, lineH, lineW);
//        if(gm.getRemainingGuesses() <= 6)
//        gc.fillRect(200,100, lineW1, lineH1);
//        if(gm.getRemainingGuesses() <= 5)
//        gc.fillOval(178, 120, 50, 50);
//        if(gm.getRemainingGuesses() <= 4)
//        gc.fillRect(200, 170, lineW, 100);
////        gc.setLineWidth(2.5);
//        if(gm.getRemainingGuesses() <= 3)
//        gc.strokeLine(200, 200, 150, 280);
////        gc.setLineWidth(2.5);
//        if(gm.getRemainingGuesses() <= 2)
//        gc.strokeLine(202, 200, 255, 280);
//        if(gm.getRemainingGuesses() <= 1)
//        gc.strokeLine(202, 200, 255, 280);


        figure.getChildren().addAll(canvas);
    }

    public static void draw(int j){
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gm = (GameData) appTemplate.getDataComponent();
        BorderPane figure = gameWorkspace.figurePane;
//        figure.setStyle("-fx-background-color: red;");
        canvas = new Canvas(450, 400);
//        figure.getChildren().addAll(canvas);
        gc = canvas.getGraphicsContext2D();
//        canvas.setStyle("-fx-background-color: blue");

//        gc.setColor(Color.BLACK);

//        if(gm.getRemainingGuesses() < 9)
//            gc.drawRect(10,70, 50, 5);
//        if(gm.getRemainingGuesses() < 8)
//            gc.drawLine(35, 70, 35, 5);
//        if(gm.getRemainingGuesses() < 7)
//            gc.drawLine(35,5,70,5);
//        if(gm.getRemainingGuesses() < 6)
//            gc.drawLine(70,60,90,65);
//        if(gm.getRemainingGuesses() < 5)
//            gc.drawLine(70, 30, 50, 25);
//        if(gm.getRemainingGuesses() < 4)
//            gc.drawLine(70,60,50,65);
//            gc.drawLine(70, 30, 90, 25);
//        if(gm.getRemainingGuesses() < 3)
//            gc.drawLine(70,60,90,65);
//        if(gm.getRemainingGuesses() < 2)
//            gc.drawLine(70, 30, 70, 60);
//        if(gm.getRemainingGuesses() < 1)
//            gc.drawLine(70, 30, 90, 25);

        String hangmanLine = "#000000";
//        int lineW = 4;
//        int lineH = 150;
        gc.setFill(Paint.valueOf(hangmanLine));
        gc.beginPath();
        gc.stroke();
        gc.setLineWidth(2.5);
        if(j <= 9) {
            gc.strokeLine(37, 360, 278, 360);
        }
        if(j <= 8){
            gc.strokeLine(105, 360, 105, 90);
        }
        if(j <= 7){
            gc.strokeLine(105, 90, 210, 90);
        }
        if(j <= 6){
            gc.strokeLine(210, 90, 210, 110);
        }
        if(j <= 5){
            gc.strokeOval(186, 110, 50, 50);
        }
        if(j <= 4){
            gc.strokeLine(210, 160, 210, 245);
        }
        if(j <= 3){
            gc.strokeLine(210, 190, 170, 230);
        }
        if(j <= 2){
            gc.strokeLine(210, 190, 250, 230);
        }
        if(j <= 1){
            gc.strokeLine(210, 245, 170, 285);
        }
        if(j <= 0){
            gc.strokeLine(210, 245, 250, 285);
        }
        figure.getChildren().addAll(canvas);
    }


    public static void clearCanvas() {
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane figure = gameWorkspace.figurePane;
        figure.getChildren().clear();
     //   gc.clearRect(450, 400, canvas.getWidth(), canvas.getHeight());
      //  gc.fill();
    }



//    public static void renderShapeRedBoxMan(Graphics gc) {
//        String hangmanLine = "#000000";
//        int lineW = 4;
//        int lineH = 150;
//
//        gc.setFill(Paint.valueOf(hangmanLine));
////        gc.fillRect(100, 100, lineW, lineH);
//        gc.beginPath();
//        gc.stroke();
//
//        gc.setColor(Color.BLACK);
//        gc.drawRect(10,70, 50, 5);
//        gc.drawLine(35, 70, 35, 5);
//        gc.drawLine(35,5,70,5);
//
//        if(hg.getWrongAttemptsLeft() < 6)
//            gc.drawOval(60, 10, 20, 20);
//        if(hg.getWrongAttemptsLeft() < 5)
//            gc.drawLine(70, 30, 70, 60);
//        if(hg.getWrongAttemptsLeft() < 4)
//            gc.drawLine(70,60,50,65);
//        if(hg.getWrongAttemptsLeft() < 3)
//            gc.drawLine(70,60,90,65);
//        if(hg.getWrongAttemptsLeft() < 2)
//            gc.drawLine(70, 30, 50, 25);
//        if(hg.getWrongAttemptsLeft() < 1)
//            gc.drawLine(70, 30, 90, 25);
//
////        gc.setFill(Paint.valueOf(hangmanLine));
////        gc.fillRect(100, 250, lineW, lineH);
////
////        gc.setFill(Paint.valueOf(hangmanLine));
////        gc.fillRect(100, 100, lineH, lineW);
////
////        int lineW1 = 3;
////        int lineH1 = 25;
////
////        gc.setFill(Paint.valueOf(hangmanLine));
////        gc.fillRect(200,100, lineW1, lineH1);
////
////        gc.fillOval(178, 120, 50, 50);
////
////        gc.fillRect(200, 170, lineW, lineH);
////
////        gc.strokeLine(200, 200, 250, 250);
////
////        gc.strokeLine(200, 200, 160, 250);
//
//    }

//    public static void clearCanvas() {
//        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
//    }

}
