package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import gui.graphicsUtil;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import sun.text.normalizer.Trie;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.*;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Stream;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private Label       goodGuess;
    private Label       badGuess;
    private HBox guessedLetters;
    private Button btn;
    private static File targetFile;
    private static File file, file1, file2, file3;
    private static Properties properties;
    private static Properties prop, prop1, prop2, prop3;
    private static String newLine = System.lineSeparator();
    private static final NavigableSet<String> dictionary;
    private final Map<Character, List<Character>> graph = (Map<Character, List<Character>>) new HashMap<Character, List<Character>>();
    Trie lexicon;
    static {
        dictionary = new TreeSet<String>();
        try {
            FileReader fr = new FileReader("/Users/Dhyan/Documents/Dhyan/ComputerScience/CSE219/Buzzword/Hangman/words");
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                dictionary.add(line.split(":")[0]);
            }
        } catch (Exception e) {
            System.err.println(
                    "Unable to read file.");
        }
    }

    static {
        targetFile = new File("./abc.txt");

        properties = new Properties();

        try
        {
            properties.load(
                    new FileInputStream(
                            targetFile.getAbsolutePath()));
        }

        catch(IOException ioe)
        {
            System.err.println(
                    "Unable to read file.");
        }
    }

    static {
        file = new File("./Dictionary.txt");

        prop = new Properties();

        try
        {
            prop.load(
                    new FileInputStream(
                            file.getAbsolutePath()));
        }

        catch(IOException ioe)
        {
            System.err.println(
                    "Unable to read file.");
        }
    }

    static {
        file2 = new File("./UsCities.txt");

        prop1 = new Properties();

        try
        {
            prop1.load(
                    new FileInputStream(
                            file2.getAbsolutePath()));
        }

        catch(IOException ioe)
        {
            System.err.println(
                    "Unable to read file.");
        }
    }

    static {
        file3 = new File("./Animals.txt");

        prop2 = new Properties();

        try
        {
            prop2.load(
                    new FileInputStream(
                            file3.getAbsolutePath()));
        }

        catch(IOException ioe)
        {
            System.err.println(
                    "Unable to read file.");
        }
    }

    static {
        file1 = new File("./Fruits.txt");

        prop3 = new Properties();

        try
        {
            prop3.load(
                    new FileInputStream(
                            file1.getAbsolutePath()));
        }

        catch(IOException ioe)
        {
            System.err.println(
                    "Unable to read file.");
        }
    }



    public void saveDictionary(String username, String category, String level) throws IOException {
        if(!file.exists())
            file.createNewFile();

        Boolean doesTheKeyValuePairExist =
                checkIfKeyValuePairExistsInDictionary(
                        username, category, level);

        if(doesTheKeyValuePairExist)
            System.err.println("Sorry, can't do it!");
        else {
            try {
                int x = getLevelDictionary(username, category);
                if (x < Integer.parseInt(level)) {
                addNewCredentialsToDictionary(
                        username, category, level);
                prop.clear();
                prop.load(new FileInputStream(file.getAbsolutePath()));
                System.out.println(
                        "Valid stuff, yo!");
            }
        }

            catch(IOException ioe)
            {
                System.err.println(
                        "Houston? You there?");
            }
        }
                }

    private void addNewCredentialsToDictionary(String username, String category, String level) throws IOException {
        FileWriter writer =
        new FileWriter(
        file.getAbsolutePath()
        , true);
        BufferedWriter buffered_writer =
        new BufferedWriter(writer);
        buffered_writer.write(
        newLine + username + ":" + category + "-" + level);
        buffered_writer.close();
        }

    public Boolean checkIfKeyValuePairExistsInDictionary(String username, String category, String level) {
    for(String key:
            prop.stringPropertyNames())
        if(key.equals(username)
                && prop.getProperty(key)
                .equals(category +":"+ level))
//                && prop.getProperty(key)
//                .equals(level))
        return true;

        return false;
        }

    public void saveUsCities(String username, String category, String level) throws IOException {
        if(!file2.exists())
            file2.createNewFile();

        Boolean doesTheKeyValuePairExist =
                checkIfKeyValuePairExistsInUsCities(
                        username, category, level);

        if(doesTheKeyValuePairExist)
            System.err.println("Sorry, can't do it!");

        else
        {
            try
            {int x = getLevelUsCities(username, category);
                if (x < Integer.parseInt(level)) {
                    addNewCredentialsToUsCities(
                            username, category, level);
                    prop1.clear();
                    prop1.load(new FileInputStream(file2.getAbsolutePath()));
                    System.out.println(
                            "Valid stuff, yo!");
                }
            }

            catch(IOException ioe)
            {
                System.err.println(
                        "Houston? You there?");
            }
        }
    }

    private void addNewCredentialsToUsCities(String username, String category, String level) throws IOException {
        FileWriter writer =
                new FileWriter(
                        file2.getAbsolutePath()
                        , true);
        BufferedWriter buffered_writer =
                new BufferedWriter(writer);
        buffered_writer.write(
                newLine + username + ":" + category + "-" + level);
        buffered_writer.close();
    }

    public Boolean checkIfKeyValuePairExistsInUsCities(String username, String category, String level) {
        for(String key:
                prop1.stringPropertyNames())
            if(key.equals(username)
                    && prop1.getProperty(key)
                    .equals(category +":"+ level))
                return true;

        return false;
    }

    public void saveAnimals(String username, String category, String level) throws IOException {
        if(!file3.exists())
            file3.createNewFile();

        Boolean doesTheKeyValuePairExist =
                checkIfKeyValuePairExistsInAnimals(
                        username, category, level);

        if(doesTheKeyValuePairExist)
            System.err.println("Sorry, can't do it!");

        else
        {
            try{
            int x = getLevelAnimals(username, category);
            if (x < Integer.parseInt(level)) {
                addNewCredentialsToAnimals(
                        username, category, level);
                prop2.clear();
                prop2.load(new FileInputStream(file3.getAbsolutePath()));
                System.out.println(
                        "Valid stuff, yo!");
            }
        }

            catch(IOException ioe)
            {
                System.err.println(
                        "Houston? You there?");
            }
        }
    }

    private void addNewCredentialsToAnimals(String username, String category, String level) throws IOException {
        FileWriter writer =
                new FileWriter(
                        file3.getAbsolutePath()
                        , true);
        BufferedWriter buffered_writer =
                new BufferedWriter(writer);
        buffered_writer.write(
                newLine + username + ":" + category + "-" + level);
        buffered_writer.close();
    }

    public Boolean checkIfKeyValuePairExistsInAnimals(String username, String category, String level) {
        for(String key:
                prop2.stringPropertyNames())
            if(key.equals(username)
                    && prop2.getProperty(key)
                    .equals(category +":"+ level))
                return true;

        return false;
    }

    public void saveFruits(String username, String category, String level) throws IOException {
        if(!file1.exists())
            file1.createNewFile();

        Boolean doesTheKeyValuePairExist =
                checkIfKeyValuePairExistsInFruits(
                        username, category, level);

        if(doesTheKeyValuePairExist)
            System.err.println("Sorry, can't do it!");

        else
        {
            try
            {
            int x = getLevelFruits(username, category);
            if (x < Integer.parseInt(level)) {
                addNewCredentialsToFruits(
                        username, category, level);
                prop3.clear();
                prop3.load(new FileInputStream(file1.getAbsolutePath()));
                System.out.println(
                        "Valid stuff, yo!");
            }
        }

            catch(IOException ioe)
            {
                System.err.println(
                        "Houston? You there?");
            }
        }
    }

    private void addNewCredentialsToFruits(String username, String category, String level) throws IOException {
        FileWriter writer =
                new FileWriter(
                        file1.getAbsolutePath()
                        , true);
        BufferedWriter buffered_writer =
                new BufferedWriter(writer);
        buffered_writer.write(
                newLine + username + ":" + category + "-" + level);
        buffered_writer.close();
    }

    public Boolean checkIfKeyValuePairExistsInFruits(String username, String category, String level) {
        for(String key:
                prop3.stringPropertyNames())
            if(key.equals(username)
                    && prop3.getProperty(key)
                    .equals(category +":"+ level))
                return true;

        return false;
    }

    public int getLevelDictionary(String username, String category){
        int level = -1;
        for(String key:
                prop.stringPropertyNames())
            if(key.equals(username))
            {
                String[] parts = prop.getProperty(key).split("-");
                String part1 = parts[0];
                String part2 = parts[1];
                if(part1.equals(category)){
                    level = Integer.parseInt(part2);

                }
            }
        return level;
    }

    public int getLevelUsCities(String username, String category){
        int level = -1;
        for(String key:
                prop1.stringPropertyNames())
            if(key.equals(username))
            {
                String[] parts = prop1.getProperty(key).split("-");
                String part1 = parts[0];
                String part2 = parts[1];
                if(part1.equals(category)){
                    level = Integer.parseInt(part2);

                }
            }
        return level;
    }

    public int getLevelAnimals(String username, String category){
        int level = -1;
        for(String key:
                prop2.stringPropertyNames())
            if(key.equals(username))
            {
                String[] parts = prop2.getProperty(key).split("-");
                String part1 = parts[0];
                String part2 = parts[1];
                if(part1.equals(category)){
                    level = Integer.parseInt(part2);

                }
            }
        return level;
    }

    public int getLevelFruits(String username, String category){
        int level = -1;
        for(String key:
                prop3.stringPropertyNames())
            if(key.equals(username))
            {
                String[] parts = prop3.getProperty(key).split("-");
                String part1 = parts[0];
                String part2 = parts[1];
                if(part1.equals(category)){
                    level = Integer.parseInt(part2);

                }
            }
        return level;
    }


    public void save(String username, String password) throws IOException {
        if(!targetFile.exists())
            targetFile.createNewFile();

        Boolean doesTheKeyValuePairExist =
                checkIfKeyValuePairExists(
                        username, password);

        if(doesTheKeyValuePairExist)
            System.err.println("Sorry, can't do it!");

        else
        {
            try
            {
                addNewCredentials(
                        username, password);
                properties.clear();
                properties.load(new FileInputStream(targetFile.getAbsolutePath()));
                System.out.println(
                        "Valid stuff, yo!");
            }

            catch(IOException ioe)
            {
                System.err.println(
                        "Houston? You there?");
            }
        }
    }

    private static void addNewCredentials(
            String username, String password)
            throws IOException
    {
        FileWriter writer =
                new FileWriter(
                        targetFile.getAbsolutePath()
                        , true);
        BufferedWriter buffered_writer =
                new BufferedWriter(writer);
        buffered_writer.write(
                newLine + username + ":" + password);
        buffered_writer.close();
    }

    public Boolean checkIfKeyValuePairExists(String username, String password) {
        for(String key:
                properties.stringPropertyNames())
            if(key.equals(username)
                    && properties.getProperty(key)
                    .equals(password))
                return true;

        return false;
    }

    public void update(String username, String password) throws IOException {
        FileWriter writer =
                new FileWriter(
                        targetFile.getAbsolutePath()
                        , true);
        BufferedWriter buffered_writer =
                new BufferedWriter(writer);
        buffered_writer.write(
                newLine + username + ":" + password);
        buffered_writer.close();
        properties.clear();
        properties.load(new FileInputStream(targetFile.getAbsolutePath()));
    }


    public String getDataFromDictionary(){
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(109583);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public String Dictionary(){
        String x = getDataFromDictionary();
        String y = onlyLettersForDictionary(x);
        int i = 0;
        if(x.length() < 15){
            int need = 15 - x.length();
            while (i <= need){
                int choice = new Random().nextInt((90 - 65) + 1) + 65;
                y += String.valueOf((char)choice);
                y.toUpperCase();
                i++;
            }
        }
        return y;
    }

    public String getDataFromUsCities(){
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(109583);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public String UsCities(){
        String x = getDataFromUsCities();
        String y = onlyLettersForUsCities(x);
        int i = 0;
        if(x.length() < 15){
            int need = 15 - x.length();
            while (i <= need){
                int choice = new Random().nextInt((90 - 65) + 1) + 65;
                y += String.valueOf((char)choice);
                y.toUpperCase();
                i++;
            }
        }
        return y;
    }

    public String getDataFromAnimals(){
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(109583);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public String Animals(){
        String x = getDataFromAnimals();
        String y = onlyLettersForAnimals(x);
        int i = 0;
        if(x.length() < 15){
            int need = 15 - x.length();
            while (i <= need){
                int choice = new Random().nextInt((90 - 65) + 1) + 65;
                y += String.valueOf((char)choice);
                y.toUpperCase();
                i++;
            }
        }
        return y;
    }

    public String getDataFromFruits(){
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(109583);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public String Fruits(){
        String x = getDataFromFruits();
        String y = onlyLettersForFruits(x);
        int i = 0;
        if(x.length() < 15){
            int need = 15 - x.length();
            while (i <= need){
                int choice = new Random().nextInt((90 - 65) + 1) + 65;
                y += String.valueOf((char)choice);
                y.toUpperCase();
                i++;
            }
        }
        return y;
    }

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {

//        char[] targetword = gamedata.getTargetWord().toCharArray();
//
////        final HBox[] hbox = new HBox[targetword.length];
//        progress = new Text[targetword.length];

        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        goodGuess = new Label((gamedata.getGoodGuesses1()));
        badGuess = new Label((gamedata.getBadGuesses1()));
        btn = new Button("Hint");
        btn.setPadding(new Insets(5, 5, 5, 5));
        graphicsUtil.Init(appTemplate);
        if(gamedata.getTargetWord().length() > 7 && !gamedata.Hint){
            btn.setVisible(true);
        }else {
            btn.setVisible(false);
        }
        btn.setOnAction(event -> {
            Hint();
        });
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        remainingGuessBox.getChildren().addAll(new Label("\tGood Guesses: "), goodGuess);
        remainingGuessBox.getChildren().addAll(new Label("\tBad Guesses: "), badGuess);
        remainingGuessBox.getChildren().addAll(new Label("\t") , btn, new Label(("\t")));
        initWordGraphics(guessedLetters,false);
//        gu.clearCanvas();
        play();
    }
    private void Hint(){
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        try{
            guessedLetters.getChildren().clear();
        } catch(Exception e) {
        }


        char[] targetword = gamedata.getTargetWord().toCharArray();

        final HBox[] hbox = new HBox[targetword.length];
        progress = new Text[targetword.length];

        for(int j=1;j<= gamedata.getBadGuesses().size();j++){
            graphicsUtil.draw((gamedata.getRemainingGuesses()+gamedata.getBadGuesses().size())-j);
        }
        for (int i = 0; i < progress.length; i++) {


                progress[i] = new Text(Character.toString(targetword[i]));
                progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
                hbox[i] = new HBox();
                hbox[i].setPadding(new javafx.geometry.Insets(0, 10, 0, 10));
                hbox[i].getChildren().add(progress[i]);
                hbox[i].setStyle("-fx-border-color: black;");
                hbox[i].setMaxWidth(20);
                hbox[i].setMaxHeight(20);

            //}

        }

        for (int i = 0; i < progress.length; i++) {
            char c = gamedata.getTargetWord().charAt(i);
            if (!gamedata.getGoodGuesses().contains(c)) {
                for(int j=0;j<progress.length;j++) {
                    if(gamedata.getTargetWord().charAt(j)  == c){
                    progress[j] = new Text(Character.toString(c));
                    progress[j].setVisible(true);
                    hbox[j] = new HBox();
                    hbox[j].setPadding(new javafx.geometry.Insets(0, 10, 0, 10));
                    hbox[j].getChildren().add(progress[i]);
                    hbox[j].setStyle("-fx-border-color: black;");
                    hbox[j].setMaxWidth(20);

                    hbox[j].setMaxHeight(20);
                    }
                }
                gamedata.addGoodGuess(targetword[i]);
                goodGuess.setText(goodGuess.getText() + " " + Character.toString(targetword[i]));
                gamedata.Hint = true;
                btn.setVisible(false);
                break;
            }
            //}

        }
        try{
            guessedLetters.getChildren().addAll(hbox);
            gamedata.useHint();
//            play();
            restoreGUI();
            graphicsUtil.draw();
        } catch(Exception e) {
        }
    }

    private void end() {
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
//            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();

//            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            remainingGuessBox.getChildren().clear();
            HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

            if (!success) {
                initWordGraphics(guessedLetters,true);
//                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
            }
            
            remainingGuessBox.getChildren().addAll(new Label(success ? " You win!\t" : " You lost!\t")) ;

//            if (success = ){
//                remainingGuessBox.getChildren().addAll(new Label("You Win!"));
//            } else {
//                remainingGuessBox.getChildren().addAll(new Label("You Lost!"));
//            }
            if (dialog.isShowing())
                dialog.toFront();

//                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }


    private void initWordGraphics(HBox guessedLetters,boolean state) {
        discovered = 0;
        try{
            //Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            //HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
            guessedLetters.getChildren().clear();

        } catch(Exception e) {
        }


        char[] targetword = gamedata.getTargetWord().toCharArray();

        final HBox[] hbox = new HBox[targetword.length];
        progress = new Text[targetword.length];

        for(int j=1;j<= gamedata.getBadGuesses().size();j++){
            graphicsUtil.draw((gamedata.getRemainingGuesses()+gamedata.getBadGuesses().size())-j);
        }
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            if(!state)
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));

            else{
                progress[i].setVisible(true);
            }
            hbox[i] = new HBox();
            hbox[i].setPadding(new javafx.geometry.Insets(0, 10, 0, 10));
            hbox[i].getChildren().add(progress[i]);
            hbox[i].setStyle("-fx-border-color: black;");
            hbox[i].setMaxWidth(20);
            hbox[i].setMaxHeight(20);
        }
        try{
            guessedLetters.getChildren().addAll(hbox);
        } catch(Exception e) {
        }

    }
//        char[] targetword = gamedata.getTargetWord().toCharArray();
//        progress = new Text[targetword.length];
//        for (int i = 0; i < progress.length; i++) {
//            progress[i] = new Text(Character.toString(targetword[i]));
//            progress[i].setVisible(false);
//        }
//        guessedLetters.getChildren().addAll(progress);


    public void play() {

        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
//                graphicsUtil.draw();
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = ' ';
                    try{
                        guess = event.getCharacter().toLowerCase().charAt(0);
                    } catch (Exception e){

                    }

                    if(guess < 97 || guess > 125)
                        return;

                    if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);


                                goodguess = true;
                                if(!gamedata.getGoodGuesses().contains(guess)){
                                    goodGuess.setText(goodGuess.getText() + " " + Character.toString(guess));
                                }

                                gamedata.addGoodGuess(guess);
                                discovered++;
                            }
                        }
                        if (!goodguess) {
                            gamedata.addBadGuess(guess);
                            badGuess.setText(badGuess.getText() + " " + Character.toString(guess));
                        }
                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                        graphicsUtil.draw();
                    }
                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {
        graphicsUtil.clearCanvas();
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedLetters);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        goodGuess = new Label((gamedata.getGoodGuesses1()));
        badGuess = new Label((gamedata.getBadGuesses1()));

        btn = new Button("Hint");
        btn.setPadding(new Insets(5, 5, 5, 5));
        if(gamedata.getTargetWord().length() > 7 && !gamedata.Hint){
            btn.setVisible(true);
        }else {
            btn.setVisible(false);
        }
        btn.setOnAction(event -> {
            Hint();
        });
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        remainingGuessBox.getChildren().addAll(new Label("\tGood Guesses: "), goodGuess);
        remainingGuessBox.getChildren().addAll(new Label("\tBad Guesses: "), badGuess);
        remainingGuessBox.getChildren().addAll(new Label("\t") , btn, new Label(("\t")));


        initWordGraphics(guessedLetters,false);

        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();

        final HBox[] hbox = new HBox[targetword.length];
        progress = new Text[targetword.length];

        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            hbox[i] = new HBox();
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            hbox[i].setPadding(new javafx.geometry.Insets(0, 10, 0, 10));
            hbox[i].getChildren().add(progress[i]);
            hbox[i].setStyle("-fx-border-color: black;");
            hbox[i].setMaxWidth(20);
            hbox[i].setMaxHeight(20);
        }
        guessedLetters.getChildren().addAll(hbox);



//
//       // progress = new Text[targetword.length];
////        progress[0].setStyle("-fx-border-color: Black;");
//        for (int i = 0; i < progress.length; i++) {
//            progress[i] = new Text(Character.toString(targetword[i]));
//            progress[i].setStyle("-fx-border-color: Black;");
//            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
//            if (progress[i].isVisible())
//                discovered++;
//        }
      //  guessedLetters.getChildren().addAll(progress);
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
//        gu.clearCanvas();
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
//                gu.clearCanvas();
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
//            gu.clearCanvas();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser filechooser = new FileChooser();
            Path appDirPath = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    public String onlyLettersForDictionary(String test){
        String line = test;
        if(line.length() <= 15 && line.length() >= 3) {
            char[] chars = line.toCharArray();

            for (char L : chars) {
                if (!Character.isLetter(L)) {
                    line = Fruits();
                    return onlyLettersForDictionary(getDataFromDictionary());
                }
            }
        } else if(line.length() > 15){
            return onlyLettersForDictionary(getDataFromDictionary());
        }
        System.out.println(line);
        return line.toUpperCase();
    }

    public String randomLetters(){
        String str ="";

        for(int i=0; i <16; i++){
         int choice = new Random().nextInt((90-65) + 1) + 65;
         str += String.valueOf((char) choice);
        }


        return str.toUpperCase();

    }

    public String onlyLettersForUsCities(String test){
        String line = test;
        if(line.length() <= 15 && line.length() >= 3) {
            char[] chars = line.toCharArray();

            for (char L : chars) {
                if (!Character.isLetter(L)) {
                    line = Fruits();
                    return onlyLettersForDictionary(getDataFromDictionary());
                }
            }
        } else if(line.length() > 15){
            return onlyLettersForDictionary(getDataFromDictionary());
        }
        System.out.println(line);
        return line.toUpperCase();
    }




    public String onlyLettersForAnimals(String test){
        String line = test;
        if(line.length() <= 15 && line.length() >= 3) {
            char[] chars = line.toCharArray();

            for (char L : chars) {
                if (!Character.isLetter(L)) {
                    line = Fruits();
                    return onlyLettersForDictionary(getDataFromDictionary());
                }
            }
        } else if(line.length() > 15){
            return onlyLettersForDictionary(getDataFromDictionary());
        }
        System.out.println(line);
        return line.toUpperCase();
    }

    public String onlyLettersForFruits(String test){
        String line = test;
        if(line.length() <= 15 && line.length() >= 3) {
            char[] chars = line.toCharArray();

            for (char L : chars) {
                if (!Character.isLetter(L)) {
                    line = Fruits();
                    return onlyLettersForDictionary(getDataFromDictionary());
                }
            }
        } else if(line.length() > 15){
            return onlyLettersForDictionary(getDataFromDictionary());
        }
        System.out.println(line);
        return line.toUpperCase();
    }

    public List<String> boggleSolver(char[][] m) {

        if (m == null) {
            throw new NullPointerException("The matrix cannot be null");
        }
        final List<String> validWords = new ArrayList<String>();

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                solve(m, i, j, m[i][j] + "", validWords);
//                solver(m, boo[m][i], lexicon, "", i, j, validWords);
            }
        }
        return validWords;
    }

    private static void solve(char[][] m, int i, int j, String prefix, List<String> validWords) {
        assert m != null;
        assert validWords != null;

        for (int i1 = Math.max(0, i - 1); i1 < Math.min(m.length, i + 2); i1++) {
            for (int j1 = Math.max(0, j - 1); j1 < Math.min(m[0].length, j + 2); j1++) {
                if (i1 != i || j1 != j) {
                    String word = prefix+ m[i1][j1];

                    if (dictionary.contains(word)) {
                        if(word.length() <= 15 && word.length() >= 3)
                        validWords.add(word);
                    }

                    if (dictionary.subSet(word, word + Character.MAX_VALUE).size() > 0) {
                        solve(m, i1, j1, word, validWords);
                    }
                }
            }
        }
    }

    public static String md5(String input) {

        String md5 = null;

        if(null == input) return null;

        try {

            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }
        return md5;
    }
}
